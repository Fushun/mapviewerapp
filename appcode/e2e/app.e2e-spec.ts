import { AppcodePage } from './app.po';

describe('appcode App', () => {
  let page: AppcodePage;

  beforeEach(() => {
    page = new AppcodePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
